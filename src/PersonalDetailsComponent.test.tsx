import { shallow, ShallowWrapper } from 'enzyme';
import { PersonalDetailsComponent } from './PersonalDetailsComponent';

describe('PersonalDetailsComponent', (): void => {
	it('renders', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<PersonalDetailsComponent
				personalDetails={{
					firstName: 'Test',
					gender: 'M',
					lastName: 'Testowy',
					pesel: '98765432'
				}}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
