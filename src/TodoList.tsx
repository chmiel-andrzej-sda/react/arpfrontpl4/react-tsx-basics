import React from 'react';
import { BackButton } from './BackButton';
import { TaskInput } from './TaskInput';
import { TodoItem } from './TodoItem';
import { TodoItemsList } from './TodoItemsList';

export function TodoList(): JSX.Element {
	const [tasks, setTasks] = React.useState<TodoItem[]>([]);

	function handleAdd(task: TodoItem): void {
		setTasks([...tasks, task]);
	}

	function handleRemoveClick(index: number): void {
		const task: TodoItem = tasks[index];
		tasks[index] = {
			...task,
			removed: !task.removed
		};
		setTasks([...tasks]);
	}

	function handleDeleteClick(index: number): void {
		tasks.splice(index, 1);
		setTasks([...tasks]);
	}

	return (
		<div>
			<BackButton to='/exercises/' />
			<h1>ToDo list</h1>
			<TodoItemsList
				className='todo-items-list'
				tasks={tasks}
				onRemoveClick={handleRemoveClick}
				onDeleteClick={handleDeleteClick}
			/>
			<TaskInput
				className='todo-task-input'
				onAdd={handleAdd}
			/>
		</div>
	);
}
