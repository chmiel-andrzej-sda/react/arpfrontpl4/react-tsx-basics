import { shallow, ShallowWrapper } from 'enzyme';
import { DynamicSelect } from './DynamicSelect';

describe('DynamicSelect', (): void => {
	it('renders', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(<DynamicSelect />);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles input', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(<DynamicSelect />);

		// when
		wrapper.find('.text-dynamic').simulate('change', {
			target: {
				value: 'test'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles button click', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(<DynamicSelect />);
		wrapper.find('.text-dynamic').simulate('change', {
			target: {
				value: 'test'
			}
		});

		// when
		wrapper.find('.button-dynamic').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles select change', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(<DynamicSelect />);
		wrapper.find('.text-dynamic').simulate('change', {
			target: {
				value: 'test'
			}
		});
		wrapper.find('.button-dynamic').simulate('click');

		wrapper.find('.text-dynamic').simulate('change', {
			target: {
				value: 'abc'
			}
		});
		wrapper.find('.button-dynamic').simulate('click');

		// when
		wrapper.find('#demo-simple-select').simulate('change', {
			target: {
				value: 'abc'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
