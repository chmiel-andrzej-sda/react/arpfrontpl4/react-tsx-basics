import { mockUseParams } from './__mocks__/MockReactRouterDom';
import { shallow, ShallowWrapper } from 'enzyme';
import { PersonPage } from './PersonPage';

describe('PersonPage', (): void => {
	it('renders without id', (): void => {
		// given
		mockUseParams.mockReturnValue({});

		// when
		const wrapper: ShallowWrapper = shallow(
			<PersonPage
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						}
					}
				]}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with NaN', (): void => {
		// given
		mockUseParams.mockReturnValue({ id: 'a' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<PersonPage
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						},
						teleAddressData: {
							city: 'Krakow',
							country: 'PL',
							email: 'test@test.pl',
							flatNo: 1,
							houseNo: 1,
							phone: '1234',
							street: 'testowa'
						}
					}
				]}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with exceeded value', (): void => {
		// given
		mockUseParams.mockReturnValue({ id: '1' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<PersonPage
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						},
						teleAddressData: {
							city: 'Krakow',
							country: 'PL',
							email: 'test@test.pl',
							flatNo: 1,
							houseNo: 1,
							phone: '1234',
							street: 'testowa'
						}
					}
				]}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with teleAddressData', (): void => {
		// given
		mockUseParams.mockReturnValue({ id: '0' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<PersonPage
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						},
						teleAddressData: {
							city: 'Krakow',
							country: 'PL',
							email: 'test@test.pl',
							flatNo: 1,
							houseNo: 1,
							phone: '1234',
							street: 'testowa'
						}
					}
				]}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders without teleAddressData', (): void => {
		// given
		mockUseParams.mockReturnValue({ id: '0' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<PersonPage
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						}
					}
				]}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
