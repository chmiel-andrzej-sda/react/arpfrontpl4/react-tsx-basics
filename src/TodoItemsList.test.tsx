import { shallow, ShallowWrapper } from 'enzyme';
import { TodoItemsList } from './TodoItemsList';

describe('TodoItemsList', (): void => {
	it('renders empty', (): void => {
		// given
		const handleDeleteClick: jest.Mock = jest.fn();
		const handleRemoveClick: jest.Mock = jest.fn();

		// when
		const wrapper: ShallowWrapper = shallow(
			<TodoItemsList
				tasks={[]}
				onDeleteClick={handleDeleteClick}
				onRemoveClick={handleRemoveClick}
			/>
		);

		// then
		expect(handleDeleteClick).toHaveBeenCalledTimes(0);
		expect(handleRemoveClick).toHaveBeenCalledTimes(0);
		expect(wrapper).toMatchSnapshot();
	});

	it('handles remove click', (): void => {
		// given
		const handleDeleteClick: jest.Mock = jest.fn();
		const handleRemoveClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TodoItemsList
				tasks={[
					{
						created: new Date(),
						removed: true,
						text: 'test'
					}
				]}
				onDeleteClick={handleDeleteClick}
				onRemoveClick={handleRemoveClick}
			/>
		);

		// when
		wrapper.find('.task-component-0').simulate('removeClick', 0);

		// then
		expect(handleDeleteClick).toHaveBeenCalledTimes(0);
		expect(handleRemoveClick).toHaveBeenCalledTimes(1);
		expect(handleRemoveClick).toHaveBeenCalledWith(0);
	});

	it('handles delete click', (): void => {
		// given
		const handleDeleteClick: jest.Mock = jest.fn();
		const handleRemoveClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TodoItemsList
				tasks={[
					{
						created: new Date(),
						removed: true,
						text: 'test'
					}
				]}
				onDeleteClick={handleDeleteClick}
				onRemoveClick={handleRemoveClick}
			/>
		);

		// when
		wrapper.find('.task-component-0').simulate('deleteClick', 0);

		// then
		expect(handleDeleteClick).toHaveBeenCalledTimes(1);
		expect(handleDeleteClick).toHaveBeenCalledWith(0);
		expect(handleRemoveClick).toHaveBeenCalledTimes(0);
	});
});
