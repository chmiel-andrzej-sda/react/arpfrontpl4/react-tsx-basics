import { shallow, ShallowWrapper } from 'enzyme';
import { Image } from './Image';

describe('Image', (): void => {
	it('renders', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<Image
				alt='test'
				src='test.png'
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
