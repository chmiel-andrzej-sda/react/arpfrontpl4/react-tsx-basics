import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { TextInput } from './TextInput';

describe('TextInput', (): void => {
	it('renders', (): void => {
		// given
		jest.spyOn(React, 'useId').mockReturnValue('1');

		// when
		const wrapper: ShallowWrapper = shallow(
			<TextInput
				label='test-label'
				onChange={(): void => undefined}
				className='test-class'
				error
				maxLength={10}
				placeholder='Test placeholder'
				value=''
				type='text'
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles change', (): void => {
		// given
		jest.spyOn(React, 'useId').mockReturnValue('1');
		const handleChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TextInput
				label='test-label'
				onChange={handleChange}
				className='test-class'
				error
				maxLength={10}
				placeholder='Test placeholder'
				value=''
				type='text'
			/>
		);

		// when
		wrapper.find('.text-input-field').simulate('change', {
			currentTarget: {
				value: 'test'
			}
		});

		// then
		expect(handleChange).toHaveBeenCalledTimes(1);
		expect(handleChange).toHaveBeenCalledWith('test');
	});
});
