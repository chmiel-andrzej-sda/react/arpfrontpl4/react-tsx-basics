import { shallow, ShallowWrapper } from 'enzyme';
import { PeopleListPage } from './PeopleListPage';

describe('PeopleListPage', (): void => {
	it('renders', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<PeopleListPage
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						}
					}
				]}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders empty', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(<PeopleListPage people={[]} />);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
