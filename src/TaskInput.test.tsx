import { shallow, ShallowWrapper } from 'enzyme';
import { TaskInput } from './TaskInput';

describe('TaskInput', (): void => {
	it('renders', (): void => {
		// given
		const handleAdd: jest.Mock = jest.fn();

		// when
		const wrapper: ShallowWrapper = shallow(
			<TaskInput
				onAdd={handleAdd}
				className='test'
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleAdd).toHaveBeenCalledTimes(0);
	});

	it('handles change', (): void => {
		// given
		const handleAdd: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TaskInput
				onAdd={handleAdd}
				className='test'
			/>
		);

		// when
		wrapper.find('#outlined-basic').simulate('change', {
			currentTarget: {
				value: 'test'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleAdd).toHaveBeenCalledTimes(0);
	});

	it('handles any key down when empty', (): void => {
		// given
		const handleAdd: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TaskInput
				onAdd={handleAdd}
				className='test'
			/>
		);

		// when
		wrapper.find('#outlined-basic').simulate('keyDown', {
			key: 'a'
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleAdd).toHaveBeenCalledTimes(0);
	});

	it('handles Enter key down when empty', (): void => {
		// given
		const handleAdd: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TaskInput
				onAdd={handleAdd}
				className='test'
			/>
		);

		// when
		wrapper.find('#outlined-basic').simulate('keyDown', {
			key: 'Enter'
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleAdd).toHaveBeenCalledTimes(0);
	});

	it('handles Enter key down when not empty', (): void => {
		// given
		const handleAdd: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TaskInput
				onAdd={handleAdd}
				className='test'
			/>
		);
		wrapper.find('#outlined-basic').simulate('change', {
			currentTarget: {
				value: 'test'
			}
		});

		// when
		wrapper.find('#outlined-basic').simulate('keyDown', {
			key: 'Enter'
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleAdd).toHaveBeenCalledTimes(1);
	});

	it('handles click when not empty', (): void => {
		// given
		const handleAdd: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TaskInput
				onAdd={handleAdd}
				className='test'
			/>
		);
		wrapper.find('#outlined-basic').simulate('change', {
			currentTarget: {
				value: 'test'
			}
		});

		// when
		wrapper.find('.task-input-button-add').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleAdd).toHaveBeenCalledTimes(1);
		expect(handleAdd).toHaveBeenCalledWith({
			text: 'test',
			removed: false,
			created: expect.any(Date)
		});
	});
});
