import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { TeleAddressData } from './TeleAddressData';

export interface TeleAddressDataComponentProps {
	readonly teleAddressData: TeleAddressData;
}

export function TeleAddressDataComponent(
	props: TeleAddressDataComponentProps
): JSX.Element {
	return (
		<TableContainer component={Paper}>
			<Table
				sx={{ minWidth: 650 }}
				aria-label='simple table'
			>
				<TableHead>
					<TableRow>
						<TableCell>phone</TableCell>
						<TableCell>email</TableCell>
						<TableCell>street</TableCell>
						<TableCell>city</TableCell>
						<TableCell>country</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					<TableRow>
						<TableCell>{props.teleAddressData.phone}</TableCell>
						<TableCell>{props.teleAddressData.email}</TableCell>
						<TableCell>
							{props.teleAddressData.street}{' '}
							{props.teleAddressData.houseNo}/
							{props.teleAddressData.flatNo}
						</TableCell>
						<TableCell>{props.teleAddressData.city}</TableCell>
						<TableCell>{props.teleAddressData.country}</TableCell>
					</TableRow>
				</TableBody>
			</Table>
		</TableContainer>
	);
}
