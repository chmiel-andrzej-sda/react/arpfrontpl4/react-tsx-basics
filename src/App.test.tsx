import { shallow, ShallowWrapper } from 'enzyme';
import App from './App';

describe('App', (): void => {
	it('renders', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(<App />);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
