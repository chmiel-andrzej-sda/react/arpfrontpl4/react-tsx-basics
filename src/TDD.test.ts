import { elementsMatch, isLenghtEven, isPalindromeArray } from './TDD';

describe('TDD', (): void => {
	describe('isLengthEven', (): void => {
		type TestSuite = {
			array?: number[];
			value: boolean;
		};
		[
			{ array: [1], value: false },
			{ array: [], value: true },
			{ array: undefined, value: false },
			{ array: [1, 1, 1], value: false },
			{ array: [1, 2], value: true }
		].forEach((element: TestSuite): void => {
			it(`[${element.array}] => ${element.value}`, (): void => {
				// when
				const result: boolean = isLenghtEven(element.array);

				// then
				expect(result).toBe(element.value);
			});
		});
	});

	describe('isPalindromeArray', (): void => {
		type TestSuite = {
			array?: number[];
			value: boolean;
		};
		[
			{ array: [1], value: true },
			{ array: [], value: true },
			{ array: undefined, value: false },
			{ array: [1, 1, 1], value: true },
			{ array: [1, 2], value: false },
			{ array: [1, 2, 1], value: true },
			{ array: [1, 1, 2], value: false },
			{ array: [1, 2, 2, 1], value: true }
		].forEach((element: TestSuite): void => {
			it(`[${element.array}] => ${element.value}`, (): void => {
				// when
				const result: boolean = isPalindromeArray(element.array);

				// then
				expect(result).toBe(element.value);
			});
		});
	});

	describe('elementsMatch', (): void => {
		type TestSuite = {
			source?: number[];
			input?: number[];
			value: boolean;
		};
		[
			{ source: undefined, input: undefined, value: false },
			{ source: undefined, input: [1], value: false },
			{ source: [], input: undefined, value: true },
			{ source: [], input: [], value: true },
			{ source: [1], input: [1], value: true },
			{ source: [1], input: [1, 1, 1], value: true },
			{ source: [1], input: [1, 2, 3], value: false },
			{ source: [1, 2], input: [], value: true },
			{ source: [1, 2], input: [1, 2], value: true },
			{ source: [1, 2], input: [2, 1], value: true },
			{ source: [1, 2], input: [1], value: true },
			{ source: [1, 2], input: [2, 2, 2, 2], value: true }
		].forEach((element: TestSuite): void => {
			it(`[${element.input}] in [${element.source}] => ${element.value}`, (): void => {
				// when
				const result: boolean = elementsMatch(
					element.source,
					element.input
				);

				// then
				expect(result).toBe(element.value);
			});
		});
	});
});
