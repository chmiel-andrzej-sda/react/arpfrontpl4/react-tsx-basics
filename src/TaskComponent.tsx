import { TodoItem } from './TodoItem';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import RestoreFromTrashIcon from '@mui/icons-material/RestoreFromTrash';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

export interface TaskComponentProps {
	task: TodoItem;
	id: number;
	onRemoveClick: (index: number) => void;
	onDeleteClick: (index: number) => void;
	className?: string;
}

export function TaskComponent(props: TaskComponentProps): JSX.Element {
	function handleRemoveClick(): void {
		props.onRemoveClick(props.id);
	}

	function handleDeleteClick(): void {
		props.onDeleteClick(props.id);
	}

	function renderText(): JSX.Element | string {
		if (props.task.removed) {
			return (
				<s>{`[${props.task.created.toUTCString()}] ${
					props.task.text
				}`}</s>
			);
		}
		return (
			<span>{`[${props.task.created.toUTCString()}] ${
				props.task.text
			}`}</span>
		);
	}

	function renderRemoveButton(): JSX.Element {
		if (props.task.removed) {
			return (
				<Button
					className='task-component-remove-button'
					onClick={handleRemoveClick}
					variant='outlined'
				>
					<RestoreFromTrashIcon />
				</Button>
			);
		}
		return (
			<Button
				className='task-component-remove-button'
				onClick={handleRemoveClick}
				variant='contained'
			>
				<DeleteIcon />
			</Button>
		);
	}

	function renderDeleteButton(): JSX.Element {
		return (
			<Button
				className='task-component-delete-button'
				onClick={handleDeleteClick}
				color='error'
				variant='contained'
			>
				<DeleteForeverIcon />
			</Button>
		);
	}

	return (
		<li>
			{renderText()} {renderRemoveButton()} {renderDeleteButton()}
		</li>
	);
}
