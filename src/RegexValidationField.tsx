import React from 'react';
import TextField from '@mui/material/TextField';

export interface RegexValidationFieldProps {
	value: RegExp;
}

export function RegexValidationField(
	props: RegexValidationFieldProps
): JSX.Element {
	const [text, setText] = React.useState<string>('');

	function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
		setText(event.currentTarget.value);
	}

	return (
		<TextField
			fullWidth
			className='regex-validation-text-field'
			label={`Type something matching ${props.value.source}`}
			variant='outlined'
			error={text !== '' && !props.value.test(text)}
			onChange={handleChange}
		/>
	);
}

// regex: new RegExp(/abc/);
// text: "abc"

// przypadki:
// 1. pusty text
// 2. poprawny tekst
// 3. nieporpawny tekst
// 4. niepoprawny -> poprawny tekst
// 5. niepoprawny -> pusty tekst
