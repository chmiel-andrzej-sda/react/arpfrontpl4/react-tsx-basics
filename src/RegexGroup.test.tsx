import { shallow, ShallowWrapper } from 'enzyme';
import { RegexGroup } from './RegexGroup';

describe('RegexGroup', (): void => {
	it('renders', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(<RegexGroup />);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handlesChange', (): void => {
		// goven
		const wrapper: ShallowWrapper = shallow(<RegexGroup />);

		// when
		wrapper
			.find('.regex-input-field')
			.simulate('change', new RegExp(/abc/));

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
