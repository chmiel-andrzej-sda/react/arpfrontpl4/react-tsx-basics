import { mockUseParams } from './__mocks__/MockReactRouterDom';
import { shallow, ShallowWrapper } from 'enzyme';
import { TeleAddressFormPage } from './TeleAddressFormPage';

describe('TeleAddressFormPage', (): void => {
	it('renders with NaN', (): void => {
		// given
		const handleModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: 'a' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				onModify={handleModify}
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						},
						teleAddressData: {
							city: 'Krakow',
							country: 'PL',
							email: 'test@test.pl',
							flatNo: 1,
							houseNo: 1,
							phone: '1234',
							street: 'testowa'
						}
					}
				]}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleModify).toHaveBeenCalledTimes(0);
	});

	it('renders with no id', (): void => {
		// given
		const handleModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({});

		// when
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				onModify={handleModify}
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						},
						teleAddressData: {
							city: 'Krakow',
							country: 'PL',
							email: 'test@test.pl',
							flatNo: 1,
							houseNo: 1,
							phone: '1234',
							street: 'testowa'
						}
					}
				]}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleModify).toHaveBeenCalledTimes(0);
	});

	it('renders with exceeded value', (): void => {
		// given
		const handleModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '1' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				onModify={handleModify}
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						},
						teleAddressData: {
							city: 'Krakow',
							country: 'PL',
							email: 'test@test.pl',
							flatNo: 1,
							houseNo: 1,
							phone: '1234',
							street: 'testowa'
						}
					}
				]}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleModify).toHaveBeenCalledTimes(0);
	});

	it('renders with correct value', (): void => {
		// given
		const handleModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '0' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				onModify={handleModify}
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						},
						teleAddressData: {
							city: 'Krakow',
							country: 'PL',
							email: 'test@test.pl',
							flatNo: 1,
							houseNo: 1,
							phone: '1234',
							street: 'testowa'
						}
					}
				]}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleModify).toHaveBeenCalledTimes(0);
	});

	it('handles button click with no data', (): void => {
		// given
		const handleModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '0' });
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				onModify={handleModify}
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'K',
							lastName: 'Testowy',
							pesel: '98765432'
						}
					}
				]}
			/>
		);

		// when
		wrapper.find('.button-send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleModify).toHaveBeenCalledTimes(0);
	});

	it('handles button click with data', (): void => {
		// given
		const handleModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '0' });
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				onModify={handleModify}
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						}
					}
				]}
			/>
		);

		wrapper.find('.input-phone').simulate('change', '123456');
		wrapper.find('.input-email').simulate('change', 'test@te.st');
		wrapper.find('.input-street').simulate('change', 'Testowa');
		wrapper.find('.input-house-no').simulate('change', '12');
		wrapper.find('.input-flat-no').simulate('change', '1');
		wrapper.find('.input-city').simulate('change', 'Krakow');
		wrapper.find('.input-country').simulate('change', 'PL');

		// when
		wrapper.find('.button-send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleModify).toHaveBeenCalledTimes(1);
		expect(handleModify).toHaveBeenCalledWith(0, {
			personalDetails: {
				firstName: 'Test',
				gender: 'M',
				lastName: 'Testowy',
				pesel: '98765432'
			},
			teleAddressData: {
				city: 'Krakow',
				country: 'PL',
				email: 'test@te.st',
				flatNo: 1,
				houseNo: 12,
				phone: '123456',
				street: 'Testowa'
			}
		});
	});

	it('handles input change', (): void => {
		// given
		const handleModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '0' });
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				onModify={handleModify}
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						}
					}
				]}
			/>
		);

		// when
		wrapper.find('.input-phone').simulate('change', '123456');
		wrapper.find('.input-email').simulate('change', 'test@te.st');
		wrapper.find('.input-street').simulate('change', 'Testowa');
		wrapper.find('.input-house-no').simulate('change', '12');
		wrapper.find('.input-flat-no').simulate('change', '1');
		wrapper.find('.input-city').simulate('change', 'Krakow');
		wrapper.find('.input-country').simulate('change', 'PL');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleModify).toHaveBeenCalledTimes(0);
	});

	it('handles button click with no data and then data', (): void => {
		// given
		const handleModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '0' });
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				onModify={handleModify}
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						}
					}
				]}
			/>
		);

		wrapper.find('.button-send').simulate('click');

		wrapper.find('.input-phone').simulate('change', '123456');
		wrapper.find('.input-email').simulate('change', 'test@te.st');
		wrapper.find('.input-street').simulate('change', 'Testowa');
		wrapper.find('.input-house-no').simulate('change', '12');
		wrapper.find('.input-flat-no').simulate('change', '1');
		wrapper.find('.input-city').simulate('change', 'Krakow');
		wrapper.find('.input-country').simulate('change', 'PL');

		// when
		wrapper.find('.button-send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleModify).toHaveBeenCalledTimes(1);
		expect(handleModify).toHaveBeenCalledWith(0, {
			personalDetails: {
				firstName: 'Test',
				gender: 'M',
				lastName: 'Testowy',
				pesel: '98765432'
			},
			teleAddressData: {
				city: 'Krakow',
				country: 'PL',
				email: 'test@te.st',
				flatNo: 1,
				houseNo: 12,
				phone: '123456',
				street: 'Testowa'
			}
		});
	});

	it('handles prevent default on form subit', (): void => {
		// given
		const handleModify: jest.Mock = jest.fn();
		const preventDefaultMock: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '0' });
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				onModify={handleModify}
				people={[
					{
						personalDetails: {
							firstName: 'Test',
							gender: 'M',
							lastName: 'Testowy',
							pesel: '98765432'
						}
					}
				]}
			/>
		);

		// when
		wrapper.find('.form-data').simulate('submit', {
			preventDefault: preventDefaultMock
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleModify).toHaveBeenCalledTimes(0);
		expect(preventDefaultMock).toHaveBeenCalledTimes(1);
	});
});
