import { shallow, ShallowWrapper } from 'enzyme';
import { RegexValidationField } from './RegexValidationField';

describe('RegexValidationField', (): void => {
	it('renders', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<RegexValidationField value={new RegExp(/abc/)} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with empty text', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<RegexValidationField value={new RegExp(/abc/)} />
		);

		// when
		wrapper.find('.regex-validation-text-field').simulate('change', {
			currentTarget: {
				value: ''
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with correct text', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<RegexValidationField value={new RegExp(/abc/)} />
		);

		// when
		wrapper.find('.regex-validation-text-field').simulate('change', {
			currentTarget: {
				value: 'abc'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with incorrect text', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<RegexValidationField value={new RegExp(/abc/)} />
		);

		// when
		wrapper.find('.regex-validation-text-field').simulate('change', {
			currentTarget: {
				value: 'apc'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with incorrect and then correct text', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<RegexValidationField value={new RegExp(/abc/)} />
		);
		wrapper.find('.regex-validation-text-field').simulate('change', {
			currentTarget: {
				value: 'apc'
			}
		});

		// when
		wrapper.find('.regex-validation-text-field').simulate('change', {
			currentTarget: {
				value: 'abc'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with incorrect and then empty text', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<RegexValidationField value={new RegExp(/abc/)} />
		);
		wrapper.find('.regex-validation-text-field').simulate('change', {
			currentTarget: {
				value: 'apc'
			}
		});

		// when
		wrapper.find('.regex-validation-text-field').simulate('change', {
			currentTarget: {
				value: ''
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
