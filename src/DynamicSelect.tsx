import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import ButtonGroup from '@mui/material/ButtonGroup';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

export function DynamicSelect(): JSX.Element {
	const [item, setItem] = React.useState<string>('');
	const [input, setInput] = React.useState<string>('');
	const [items, setItems] = React.useState<string[]>([]);

	function handleChange(event: SelectChangeEvent): void {
		setItem(event.target.value as string);
	}

	function handleInputChange(
		event: React.ChangeEvent<HTMLInputElement>
	): void {
		setInput(event.target.value as string);
	}

	function handleButtonClick(): void {
		setItems([...items, input]);
		setInput('');
	}

	return (
		<div>
			<FormControl fullWidth>
				<InputLabel id='demo-simple-select-label'>Item</InputLabel>
				<Select
					labelId='demo-simple-select-label'
					id='demo-simple-select'
					value={item}
					label='Item'
					onChange={handleChange}
				>
					{items.map(
						(item: string, index: number): JSX.Element => (
							<MenuItem
								key={index}
								value={item}
							>
								{item}
							</MenuItem>
						)
					)}
				</Select>
			</FormControl>
			<ButtonGroup variant='contained'>
				<TextField
					className='text-dynamic'
					onChange={handleInputChange}
					label='Input'
					variant='outlined'
					value={input}
				/>
				<Button
					className='button-dynamic'
					onClick={handleButtonClick}
				>
					Add
				</Button>
			</ButtonGroup>
		</div>
	);
}
