import { shallow, ShallowWrapper } from 'enzyme';
import { TodoList } from './TodoList';

describe('TodoList', (): void => {
	it('renders', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(<TodoList />);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles add', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(<TodoList />);
		const date: Date = new Date(Date.UTC(2022, 7, 7, 5, 15, 25));

		// when
		wrapper.find('.todo-task-input').simulate('add', {
			text: 'test',
			removed: false,
			created: date
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles delete', (): void => {
		// usunac jeden z wielu
		// given
		const wrapper: ShallowWrapper = shallow(<TodoList />);
		const date: Date = new Date(Date.UTC(2022, 7, 7, 5, 15, 25));
		wrapper.find('.todo-task-input').simulate('add', {
			text: 'test',
			removed: false,
			created: date
		});

		// when
		wrapper.find('.todo-items-list').simulate('deleteClick', 0);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles remove', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(<TodoList />);
		const date: Date = new Date(Date.UTC(2022, 7, 7, 5, 15, 25));
		wrapper.find('.todo-task-input').simulate('add', {
			text: 'test',
			removed: false,
			created: date
		});

		// when
		wrapper.find('.todo-items-list').simulate('removeClick', 0);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles unremove', (): void => {
		// given
		const wrapper: ShallowWrapper = shallow(<TodoList />);
		const date: Date = new Date(Date.UTC(2022, 7, 7, 5, 15, 25));
		wrapper.find('.todo-task-input').simulate('add', {
			text: 'test',
			removed: true,
			created: date
		});

		// when
		wrapper.find('.todo-items-list').simulate('removeClick', 0);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
