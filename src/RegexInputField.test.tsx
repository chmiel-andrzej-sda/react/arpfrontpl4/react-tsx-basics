import { shallow, ShallowWrapper } from 'enzyme';
import { RegexInputField } from './RegexInputField';

describe('RegexInputField', (): void => {
	it('renders', (): void => {
		// given
		const handleChange: jest.Mock = jest.fn();

		// when
		const wrapper: ShallowWrapper = shallow(
			<RegexInputField
				onChange={handleChange}
				className='test-class'
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleChange).toHaveBeenCalledTimes(0);
	});

	it('handles invalid regex', (): void => {
		// given
		const handleChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<RegexInputField
				onChange={handleChange}
				className='test-class'
			/>
		);

		// when
		wrapper.find('.regex-input-text-field').simulate('change', '/(a/');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleChange).toHaveBeenCalledTimes(0);
	});

	it('handles valid regex', (): void => {
		// given
		const handleChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<RegexInputField
				onChange={handleChange}
				className='test-class'
			/>
		);

		// when
		wrapper.find('.regex-input-text-field').simulate('change', {
			currentTarget: {
				value: '/(a)/'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleChange).toHaveBeenCalledTimes(1);
		expect(handleChange).toHaveBeenCalledWith(new RegExp('/(a)/'));
	});

	it('handles invalid and then valid regex', (): void => {
		// given
		const handleChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<RegexInputField
				onChange={handleChange}
				className='test-class'
			/>
		);
		wrapper.find('.regex-input-text-field').simulate('change', {
			currentTarget: {
				value: '/(a/'
			}
		});

		// when
		wrapper.find('.regex-input-text-field').simulate('change', {
			currentTarget: {
				value: '/(a)/'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleChange).toHaveBeenCalledTimes(1);
		expect(handleChange).toHaveBeenCalledWith(new RegExp('/(a)/'));
	});
});
