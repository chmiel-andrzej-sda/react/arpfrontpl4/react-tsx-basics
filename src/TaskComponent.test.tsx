import { shallow, ShallowWrapper } from 'enzyme';
import { TaskComponent } from './TaskComponent';

describe('TaskComponent', (): void => {
	it('renders removed', (): void => {
		// given
		const date: Date = new Date(Date.UTC(2022, 7, 7, 5, 15, 25));
		const handleRemoveClick: jest.Mock = jest.fn();
		const handleDeleteClick: jest.Mock = jest.fn();

		// when
		const wrapper: ShallowWrapper = shallow(
			<TaskComponent
				id={1}
				task={{
					text: 'task',
					created: date,
					removed: true
				}}
				onRemoveClick={handleRemoveClick}
				onDeleteClick={handleDeleteClick}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleRemoveClick).toHaveBeenCalledTimes(0);
		expect(handleDeleteClick).toHaveBeenCalledTimes(0);
	});

	it('renders unremoved', (): void => {
		// given
		const date: Date = new Date(Date.UTC(2022, 7, 7, 5, 15, 25));
		const handleRemoveClick: jest.Mock = jest.fn();
		const handleDeleteClick: jest.Mock = jest.fn();

		// when
		const wrapper: ShallowWrapper = shallow(
			<TaskComponent
				id={1}
				task={{
					text: 'task',
					created: date,
					removed: false
				}}
				onRemoveClick={handleRemoveClick}
				onDeleteClick={handleDeleteClick}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleRemoveClick).toHaveBeenCalledTimes(0);
		expect(handleDeleteClick).toHaveBeenCalledTimes(0);
	});

	it('renders unremoved', (): void => {
		// given
		const date: Date = new Date(Date.UTC(2022, 7, 7, 5, 15, 25));
		const handleRemoveClick: jest.Mock = jest.fn();
		const handleDeleteClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TaskComponent
				id={1}
				task={{
					text: 'task',
					created: date,
					removed: false
				}}
				onRemoveClick={handleRemoveClick}
				onDeleteClick={handleDeleteClick}
			/>
		);

		// when
		wrapper.find('.task-component-remove-button').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleRemoveClick).toHaveBeenCalledTimes(1);
		expect(handleRemoveClick).toHaveBeenCalledWith(1);
		expect(handleDeleteClick).toHaveBeenCalledTimes(0);
	});

	it('renders unremoved', (): void => {
		// given
		const date: Date = new Date(Date.UTC(2022, 7, 7, 5, 15, 25));
		const handleRemoveClick: jest.Mock = jest.fn();
		const handleDeleteClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TaskComponent
				id={1}
				task={{
					text: 'task',
					created: date,
					removed: false
				}}
				onRemoveClick={handleRemoveClick}
				onDeleteClick={handleDeleteClick}
			/>
		);

		// when
		wrapper.find('.task-component-delete-button').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(handleRemoveClick).toHaveBeenCalledTimes(0);
		expect(handleDeleteClick).toHaveBeenCalledTimes(1);
		expect(handleDeleteClick).toHaveBeenCalledWith(1);
	});
});
