import { shallow, ShallowWrapper } from 'enzyme';
import { SendButton } from './SendButton';

describe('SendButton', (): void => {
	it('renders', (): void => {
		// given
		const handleClick: jest.Mock = jest.fn();

		// when
		const wrapper: ShallowWrapper = shallow(
			<SendButton
				onClick={handleClick}
				className='test'
			/>
		);

		// then
		expect(handleClick).toHaveBeenCalledTimes(0);
		expect(wrapper).toMatchSnapshot();
	});

	it('handles click', (): void => {
		// given
		const handleClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<SendButton
				onClick={handleClick}
				className='test'
			/>
		);

		// when
		wrapper.find('.send-button').simulate('click'); // "click" => "Click" => "onClick"

		// then
		expect(handleClick).toHaveBeenCalledTimes(1);
	});
});
