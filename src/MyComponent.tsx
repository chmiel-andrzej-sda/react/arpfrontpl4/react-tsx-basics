import React from 'react';
import { MyButton } from './MyButton';
import { Person } from './Person';

export interface MyComponentProps {
	onAdd: (person: Person) => void;
}

export function MyComponent(props: MyComponentProps): JSX.Element {
	const [count, setCount] = React.useState<number>(0);

	React.useEffect((): (() => void) => {
		console.log(count);
		return () => console.log('now');
	});

	function handleClick(newValue: number): void {
		setCount(newValue);
		props.onAdd({
			personalDetails: {
				gender: 'M',
				firstName: 'test',
				lastName: 'test',
				pesel: '765'
			}
		});
	}

	return (
		<>
			<MyButton
				defaultLabel='Test'
				value={count}
				onClick={handleClick}
			/>
			<br />
			The value is: {count}
		</>
	);
}
