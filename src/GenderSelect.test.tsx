import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { GenderSelect } from './GenderSelect';

describe('GenderSelect', (): void => {
	it('renders', (): void => {
		// given
		jest.spyOn(React, 'useId').mockReturnValue('test-id');
		const mockChange: jest.Mock = jest.fn();

		// when
		const wrapper: ShallowWrapper = shallow(
			<GenderSelect
				label='test-label'
				genders={['M', 'K']}
				value='K'
				onChange={mockChange}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockChange).toHaveBeenCalledTimes(0);
	});

	it('handles change', (): void => {
		// given
		jest.spyOn(React, 'useId').mockReturnValue('test-id');
		const mockChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<GenderSelect
				label='test-label'
				genders={['M', 'K']}
				value='K'
				onChange={mockChange}
			/>
		);

		// when
		wrapper.find('#test-id').simulate('change', {
			target: {
				value: 'M'
			}
		});

		// then
		expect(mockChange).toHaveBeenCalledTimes(1);
		expect(mockChange).toHaveBeenCalledWith('M');
	});
});
