import React, { HTMLInputTypeAttribute } from 'react';
import TextField from '@mui/material/TextField';
import './TextInput.css';

export interface TextInputProps {
	value?: string;
	label: string;
	onChange: (value: string) => void;
	type?: HTMLInputTypeAttribute;
	maxLength?: number;
	placeholder?: string;
	error?: boolean;
	className?: string;
}

export function TextInput(props: TextInputProps) {
	const id: string = React.useId();

	function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
		props.onChange(event.currentTarget.value);
	}

	return (
		<TextField
			id={id}
			className='text-input-field'
			label={props.label}
			fullWidth
			variant='outlined'
			error={props.error}
			type={props.type}
			value={props.value}
			onChange={handleChange}
		/>
	);
}
