import { shallow, ShallowWrapper } from 'enzyme';
import { FormPage } from './FormPage';

describe('FormPage', (): void => {
	it('renders', (): void => {
		// given
		const sendClickMock: jest.Mock = jest.fn();

		// when
		const wrapper: ShallowWrapper = shallow(
			<FormPage onSendClick={sendClickMock} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(sendClickMock).toHaveBeenCalledTimes(0);
	});

	it('handles change', (): void => {
		// given
		const sendClickMock: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<FormPage onSendClick={sendClickMock} />
		);

		// when
		wrapper.find('.gender-select').simulate('change', 'K');
		wrapper.find('.input-first-name').simulate('change', 'test-first-name');
		wrapper
			.find('.input-middle-name')
			.simulate('change', 'test-middle-name');
		wrapper.find('.input-last-name').simulate('change', 'test-last-name');
		wrapper.find('.input-pesel').simulate('change', '12341234');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(sendClickMock).toHaveBeenCalledTimes(0);
	});

	it('handles form submit', (): void => {
		// given
		const sendClickMock: jest.Mock = jest.fn();
		const preventDefaultMock: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<FormPage onSendClick={sendClickMock} />
		);

		// when
		wrapper.find('.form').simulate('submit', {
			preventDefault: preventDefaultMock
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(sendClickMock).toHaveBeenCalledTimes(0);
		expect(preventDefaultMock).toHaveBeenCalledTimes(1);
	});

	it('handles incorrect data', (): void => {
		// given
		const sendClickMock: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<FormPage onSendClick={sendClickMock} />
		);

		// when
		wrapper.find('.send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(sendClickMock).toHaveBeenCalledTimes(0);
	});

	it('handles correct data', (): void => {
		// given
		const sendClickMock: jest.Mock = jest.fn().mockReturnValue(4);
		const wrapper: ShallowWrapper = shallow(
			<FormPage onSendClick={sendClickMock} />
		);

		wrapper.find('.input-first-name').simulate('change', 'test-first-name');
		wrapper
			.find('.input-middle-name')
			.simulate('change', 'test-middle-name');
		wrapper.find('.input-last-name').simulate('change', 'test-last-name');
		wrapper.find('.input-pesel').simulate('change', '12341234');

		// when
		wrapper.find('.send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(sendClickMock).toHaveBeenCalledTimes(1);
		expect(sendClickMock).toHaveBeenCalledWith({
			personalDetails: {
				firstName: 'test-first-name',
				middleName: 'test-middle-name',
				lastName: 'test-last-name',
				pesel: '12341234',
				gender: 'M'
			}
		});
	});
});
