import React from 'react';
import { Navigate, Params, useParams } from 'react-router-dom';
import { BackButton } from './BackButton';
import { Person } from './Person';
import { Gender } from './PersonalDetails';
import { SendButton } from './SendButton';
import { TextInput } from './TextInput';

export interface TeleAddressFormPageProps {
	people: Person[];
	onModify: (id: number, person: Person) => void;
}

export function TeleAddressFormPage(
	props: TeleAddressFormPageProps
): JSX.Element {
	const params: Readonly<Params<string>> = useParams();

	const [phone, setPhone] = React.useState<string>('');
	const [phoneError, setPhoneError] = React.useState<boolean>(false);
	const [email, setEmail] = React.useState<string>('');
	const [emailError, setEmailError] = React.useState<boolean>(false);
	const [street, setStreet] = React.useState<string>('');
	const [streetError, setStreetError] = React.useState<boolean>(false);
	const [houseNo, setHouseNo] = React.useState<string>('');
	const [houseNoError, setHouseNoError] = React.useState<boolean>(false);
	const [flatNo, setFlatNo] = React.useState<string>('');
	const [flatNoError, setFlatNoError] = React.useState<boolean>(false);
	const [city, setCity] = React.useState<string>('');
	const [cityError, setCityError] = React.useState<boolean>(false);
	const [country, setCountry] = React.useState<string>('');
	const [countryError, setcountryError] = React.useState<boolean>(false);

	const id: number = parseInt(params.id || '');

	if (Number.isNaN(id)) {
		return <Navigate to='/person' />;
	}

	if (id >= props.people.length) {
		return <Navigate to='/person' />;
	}

	if (props.people[id].teleAddressData) {
		return <Navigate to={`/person/${id}`} />;
	}

	const gender: Gender = props.people[id].personalDetails.gender;

	function handleFormSubmit(event: React.FormEvent): void {
		event.preventDefault();
	}

	function handleButtonClick(): void {
		if (phone === '') {
			setPhoneError(true);
		}
		if (email === '') {
			setEmailError(true);
		}
		if (street === '') {
			setStreetError(true);
		}
		if (houseNo === '') {
			setHouseNoError(true);
		}
		if (flatNo === '') {
			setFlatNoError(true);
		}
		if (city === '') {
			setCityError(true);
		}
		if (country === '') {
			setcountryError(true);
		}
		if (phone && email && street && houseNo && flatNo && city && country) {
			const newPerson: Person = {
				...props.people[id],
				teleAddressData: {
					phone,
					email,
					street,
					houseNo: parseInt(houseNo),
					flatNo: parseInt(flatNo),
					city,
					country
				}
			};
			props.onModify(id, newPerson);
		}
	}

	function handlePhoneChange(value: string): void {
		setPhone(value);
		setPhoneError(false);
	}

	function handleEmailChange(value: string): void {
		setEmail(value);
		setEmailError(false);
	}

	function handleStreetChange(value: string): void {
		setStreet(value);
		setStreetError(false);
	}

	function handleHouseNoChange(value: string): void {
		setHouseNo(value);
		setHouseNoError(false);
	}

	function handleFlatNoChange(value: string): void {
		setFlatNo(value);
		setFlatNoError(false);
	}

	function handleCityChange(value: string): void {
		setCity(value);
		setCityError(false);
	}

	function handleCountryChange(value: string): void {
		setCountry(value);
		setcountryError(false);
	}

	return (
		<>
			<BackButton to={`/person/${params.id}`} />
			<form
				className='form-data'
				onSubmit={handleFormSubmit}
			>
				<TextInput
					className='input-phone'
					label={`Jaki jest ${
						gender === 'M' ? 'Pana' : 'Pani'
					} telefon?`}
					type='tel'
					value={phone}
					onChange={handlePhoneChange}
					error={phoneError}
				/>
				<TextInput
					className='input-email'
					label={`Jaki jest ${
						gender === 'M' ? 'Pana' : 'Pani'
					} email?`}
					type='email'
					value={email}
					onChange={handleEmailChange}
					error={emailError}
				/>
				<TextInput
					className='input-street'
					label={`Jaka jest ${
						gender === 'M' ? 'Pana' : 'Pani'
					} ulica?`}
					value={street}
					onChange={handleStreetChange}
					error={streetError}
				/>
				<TextInput
					className='input-house-no'
					label={`Jaki jest ${
						gender === 'M' ? 'Pana' : 'Pani'
					} nr domu?`}
					value={houseNo}
					onChange={handleHouseNoChange}
					error={houseNoError}
				/>
				<TextInput
					className='input-flat-no'
					label={`Jaki jest ${
						gender === 'M' ? 'Pana' : 'Pani'
					} nr mieszkania?`}
					value={flatNo}
					onChange={handleFlatNoChange}
					error={flatNoError}
				/>
				<TextInput
					className='input-city'
					label={`Jakie jest ${
						gender === 'M' ? 'Pana' : 'Pani'
					} miasto?`}
					value={city}
					onChange={handleCityChange}
					error={cityError}
				/>
				<TextInput
					className='input-country'
					label={`Jaki jest ${
						gender === 'M' ? 'Pana' : 'Pani'
					} kraj?`}
					value={country}
					onChange={handleCountryChange}
					error={countryError}
				/>
				<SendButton
					className='button-send'
					onClick={handleButtonClick}
				/>
			</form>
		</>
	);
}
