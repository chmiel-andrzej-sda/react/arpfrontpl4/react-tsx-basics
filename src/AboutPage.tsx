import { BackButton } from './BackButton';

export function AboutPage(): JSX.Element {
	return (
		<div>
			<BackButton to='/' />
			<p>It&apos;s my wunderbar website!</p>
		</div>
	);
}
