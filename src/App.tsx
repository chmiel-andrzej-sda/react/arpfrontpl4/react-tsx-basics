import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { AboutPage } from './AboutPage';
import './App.css';
import { DynamicSelect } from './DynamicSelect';
import { ExercisesPage } from './ExercisesPage';
import { FormPage } from './FormPage';
import { HalfDynamicInput } from './HalfDynamicInput';
import { MainPage } from './MainPage';
import { PeopleListPage } from './PeopleListPage';
import { Person } from './Person';
import { PersonPage } from './PersonPage';
import { RegexGroup } from './RegexGroup';
import { TeleAddressFormPage } from './TeleAddressFormPage';
import { TodoList } from './TodoList';

function App(): JSX.Element {
	const [people, setPeople] = React.useState<Person[]>([
		{
			personalDetails: {
				gender: 'M',
				firstName: 'Jan',
				lastName: 'Kowalski',
				pesel: '1234'
			},
			teleAddressData: {
				city: 'Krakow',
				country: 'PL',
				email: 'test@te.st',
				flatNo: 1,
				houseNo: 2,
				phone: '123',
				street: 'Nope'
			}
		},
		{
			personalDetails: {
				gender: 'M',
				firstName: 'Jerry',
				lastName: 'Mysz',
				pesel: '345'
			}
		},
		{
			personalDetails: {
				gender: 'K',
				firstName: 'Janina',
				lastName: 'Kowalska',
				pesel: '987',
				middleName: 'Test'
			}
		}
	]);

	function handleAdd(person: Person): number {
		setPeople([...people, person]);
		return people.length;
	}

	function handleModify(id: number, person: Person): void {
		people[id] = person;
		setPeople([...people]);
	}

	return (
		<Routes>
			<Route
				path='/'
				element={<MainPage />}
			/>
			<Route
				path='/about'
				element={<AboutPage />}
			/>
			<Route
				path='/person/:id'
				element={<PersonPage people={people} />}
			/>
			<Route
				path='/person'
				element={<PeopleListPage people={people} />}
			/>
			<Route
				path='/add'
				element={<FormPage onSendClick={handleAdd} />}
			/>
			<Route
				path='/person/:id/address'
				element={
					<TeleAddressFormPage
						people={people}
						onModify={handleModify}
					/>
				}
			/>
			<Route
				path='/exercises'
				element={<ExercisesPage />}
			/>
			<Route
				path='/exercises/1'
				element={<HalfDynamicInput />}
			/>
			<Route
				path='/exercises/2'
				element={<RegexGroup />}
			/>
			<Route
				path='/exercises/3'
				element={<TodoList />}
			/>
			<Route
				path='/exercises/4'
				element={<DynamicSelect />}
			/>
		</Routes>
	);
}

export default App;
