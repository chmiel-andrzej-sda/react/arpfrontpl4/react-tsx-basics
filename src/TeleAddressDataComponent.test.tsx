import { shallow, ShallowWrapper } from 'enzyme';
import { TeleAddressDataComponent } from './TeleAddressDataComponent';

describe('TeleAddressDataComponent', (): void => {
	it('renders', (): void => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressDataComponent
				teleAddressData={{
					city: 'Krakow',
					country: 'PL',
					email: 'test@test.pl',
					flatNo: 1,
					houseNo: 1,
					phone: '1234',
					street: 'testowa'
				}}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
