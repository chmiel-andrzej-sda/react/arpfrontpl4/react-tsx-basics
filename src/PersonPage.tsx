import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
import { Navigate, Params, useParams } from 'react-router-dom';
import { BackButton } from './BackButton';
import { Person } from './Person';
import { PersonalDetailsComponent } from './PersonalDetailsComponent';
import { TeleAddressDataComponent } from './TeleAddressDataComponent';

export interface PersonPageProps {
	people: Person[];
}

export function PersonPage(props: PersonPageProps): JSX.Element {
	const params: Readonly<Params<string>> = useParams();
	const index: number = parseInt(params.id || '');

	if (Number.isNaN(index)) {
		return <Navigate to='/' />;
	}

	if (index >= props.people.length) {
		return <Navigate to='/' />;
	}

	const person: Person = props.people[index];

	function renderTeleAddressData(): JSX.Element {
		if (person.teleAddressData) {
			return (
				<TeleAddressDataComponent
					teleAddressData={person.teleAddressData}
				/>
			);
		}
		return (
			<Button
				component={Link}
				to='address'
			>
				Add teleaddress data &gt;
			</Button>
		);
	}

	return (
		<>
			<BackButton to='/person' />
			<PersonalDetailsComponent
				personalDetails={person.personalDetails}
			/>
			{renderTeleAddressData()}
		</>
	);
}
