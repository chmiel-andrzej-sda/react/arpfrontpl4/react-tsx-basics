import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';

export interface BackButtonProps {
	to: string;
}

export function BackButton(props: BackButtonProps): JSX.Element {
	return (
		<Button
			variant='outlined'
			component={Link}
			to={props.to}
		>
			&lt; WSTECZ
		</Button>
	);
}
